<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('layouts.main.partials.head')

<body>
    <div id="app" class="container">
        @include('layouts.main.partials.navbar')

        @yield('content')
    </div>
</body>
</html>
